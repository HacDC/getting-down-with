/************************************************************************
 * echo.js written by Kevin Cole <dc.loco@gmail.com> 2013.07.14         *
 *                                                                      *
 * This creates a pop-up alert box containing all data submitted from   *
 * a form for field types: input/text, input/radio, input/checkbox,     *
 * select-one (pulldown), and textarea (long text).                     *
 *                                                                      *
 * Call with:                                                           *
 *                                                                      *
 *   <head>                                                             *
 *     <script src="echo.js"></script>                                  *
 *   </head>                                                            *
 *                                                                      *
 *   <body>                                                             *
 *     <form onsubmit="return echo(this);">                             *
 *       ...                                                            *
 *       <button type="submit">...</button>                             *
 *     </form>                                                          *
 *   </body>                                                            *
 *                                                                      *
 * Caveats:                                                             *
 *    * it cannot handle multiple selections in a pulldown menu         *
 *                                                                      *
 ***********************************************************************/

function echo (myform) {
  // Initialize buffer.
  var buffer = "";

  // For each field in the form...
  for (var field_ndx=0; field_ndx < myform.length; field_ndx++) {
    var field = myform.elements[field_ndx];
   
    switch (field.type) {
      // If field is a text entry, password, or textarea...
      case "text":
      case "password":
      case "textarea":
        // ...Add the field name to output buffer
        buffer += field.name + ": ";
        // ...Add the value of the field to buffer
        buffer += field.value + "\n";
        break;
      // If field is part of a set of radio buttons...
      case "radio":
        // ...If the radio button is selected...
        if (field.checked) {
          // ......Add the field name to the buffer
          buffer += field.name  + ": ";
          // ......Add the value to the buffer
          buffer += field.value +"\n";
        }
        break;
      // If field is a checkbox...
      case "checkbox":
        // ...If the checkbox is checked...
        if (field.checked) {
          // ......Add the field name to the buffer
          buffer += field.name  + ": ";
          // ......Add the value to the buffer
          buffer += field.value +"\n";
        }
        break;
      // If field is a pull-down list...
      case "select-one":
        // ...Get the list of options
        var choices = field.options;
        // ...Find the selected item
        var ndx     = field.selectedIndex;
        // ...Add the field name to output buffer
        buffer += field.name + ": ";
        // ...Add ONLY text of selected option
        buffer += choices[ndx].text + "\n";
        break;
    };  // end switch
  };  // end for

  // pop-up the form data accumulated in buffer
  alert(buffer);
  // cancel the submit action
  return false;
}; // end echo()
